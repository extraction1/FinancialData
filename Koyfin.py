from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

# Enable headless mode in Selenium
options = Options()
options.add_argument('--headless=new')


url = "https://coinmarketcap.com/currencies/bitcoin/"

driver = webdriver.Chrome(
    options=options
)

driver.get(url)

# per_apple = driver.find_element(By.XPATH, "//*[@id=\"quote-summary\"]/div[2]/table/tbody/tr[3]/td[2]")
bitcoin_price = driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[2]/div/div/div[2]/div[1]/div[2]/span')

print(bitcoin_price.text)
